<?php

namespace SmartyConfig\Common;

class SmartyConfig
{
    public static function configDir()
    {
        $dirArray = array(
            S_ROOT . 'vendor/twcode/llll-share-config/src/SmartyConfig/Ll',
            S_ROOT . 'vendor/twcode/llll-share-config/src/SmartyConfig/Common',
            S_ROOT . 'vendor/twcode/llll-share-config/src/SmartyConfig/',
        );

        return $dirArray;
    }
}
